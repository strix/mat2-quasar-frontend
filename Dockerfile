# production container
# https://jonathanmh.com/deploying-a-vue-js-single-page-app-including-router-with-docker/
# https://www.georg-ledermann.de/blog/2018/04/27/dockerize-and-configure-javascript-single-page-application/
FROM nginx:1.17.6-alpine as production-stage
RUN mkdir -p /tmp/nginx/vue-single-page-app && \
    mkdir -p /var/log/nginx && \
    mkdir -p /var/www/html
COPY ./dist/pwa /var/www/html/
COPY nginx_config/default.conf /etc/nginx/conf.d/default.conf
COPY nginx_config/nginx.conf /etc/nginx/nginx.conf
COPY ./entrypoint.sh /tmp/
EXPOSE 80

CMD ["/tmp/entrypoint.sh"]
