# https://about.gitlab.com/2017/09/12/vuejs-app-gitlab/
# https://medium.com/joolsoftware/connect-gitlab-with-digitaloceans-kubernetes-439076b9de17
# https://blog.lwolf.org/post/how-to-create-ci-cd-pipeline-with-autodeploy-k8s-gitlab-helm/
stages:
  - build
  - review_app_build
  - review_app
  - test
  - container_sast
  - renovate

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
    - dist/pwa

.build_frontend_template: &build_frontend-template
  image: node:12
  stage: build
  variables:
    MAT2_API_URL_PROD: 'https://backend.matweb.info/'
  except:
    - schedules
  before_script:
    - yarn install
    - yarn global add @quasar/cli
    - echo $MAT2_API_URL_PROD
  script:
    - quasar build -m pwa
  artifacts:
    expire_in: 1 week
    paths:
      - dist/pwa

build_frontend:
  <<: *build_frontend-template
  variables:
    MAT2_API_URL_PROD: https://backend.matweb.info/
  only:
    - branches
    - merge_requests


build_frontend_with_placeholder_url:
  <<: *build_frontend-template
  variables:
    MAT2_API_URL_PROD: '$MAT_API_HOST_PLACEHOLDER'
  only:
    - master
    - tags
  except:
    - master

.container-build-template: &container-build-template
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://localhost:2375
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - echo $CI_COMMIT_REF_SLUG
  script:
    - docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: review_app_build

build_container:
  <<: *container-build-template
  only:
    - branches
    - merge_requests
  except:
    - master

build_latest_container:
  <<: *container-build-template
  script:
    - docker build --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:latest
  only:
    - master

build_tagged_container:
  <<: *container-build-template
  script:
    - docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  only:
    - tags

review_app_init:
  stage: review_app
  image: lachlanevenson/k8s-kubectl:v1.16.3
  before_script:
    - echo "deploying review app $CI_COMMIT_REF_SLUG"
    - apk add --no-cache gettext
  script:
    - echo $CI_ENVIRONMENT_SLUG
    - envsubst < kubernetes.template.yml > kubernetes.yml
    - cat kubernetes.yml
    - kubectl get pod -n gitlab-namespace
    - kubectl apply -f kubernetes.yml -n gitlab-namespace
    - kubectl apply -f backend.yml -n gitlab-namespace
    # force a triggered update
    - kubectl patch deployment $CI_ENVIRONMENT_SLUG-app -n gitlab-namespace -p \
      "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"$(date +%s)\"}}}}}"
    - kubectl patch deployment mat-backend -n gitlab-namespace -p \
      "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"$(date +%s)\"}}}}}"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.matweb.info
    on_stop: review_app_stop
  only:
    - branches
    - merge_requests

review_app_stop:
  stage: review_app
  image: lachlanevenson/k8s-kubectl:v1.16.3
  before_script:
    - echo "deleting review app"
    - apk add --no-cache gettext
  script:
    - envsubst < kubernetes.template.yml > kubernetes.yml
    - kubectl delete -f kubernetes.yml -n gitlab-namespace
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  only:
    - branches
    - merge_requests
  except:
    - master
    - develop

integration_test:
  image: cypress/included:3.4.1
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.matweb.info
  variables:
    CYPRESS_baseUrl: https://$CI_ENVIRONMENT_SLUG.matweb.info/
  before_script:
    - echo "running e2e tests against https://$CI_ENVIRONMENT_SLUG.matweb.info"
  stage: test
  script:
    - chmod +x wait-for-it.sh
    - export CYPRESS_baseUrl=https://$CI_ENVIRONMENT_SLUG.matweb.info
    - echo $CYPRESS_baseUrl
    - ./wait-for-it.sh -h $CI_ENVIRONMENT_SLUG.matweb.info -s -p 443 -t 0 -- cypress run --spec "**/*.spec.js"

renovate:
  stage: renovate
  image: docker:latest
  services:
      - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://localhost:2375
    RENOVATE_CONFIG_FILE: renovate-config.js
  only:
    - schedules
  before_script:
    - echo "updating deps"
  script:
    - docker run -e GITLAB_TOKEN="$GITLAB_TOKEN" -e GITHUB_TOKEN="$GITHUB_TOKEN" -v $PWD/renovate-config.js:/usr/src/app/config.js renovate/renovate:13 $(cat repositories.txt | xargs)
  allow_failure: true

container_sast:
  stage: container_sast
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://localhost:2375
  allow_failure: true
  before_script:
    - echo "Running Container SAST"
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.1
    - apk add -U wget ca-certificates
    - docker pull $CI_REGISTRY_IMAGE:latest
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - while( ! wget -q -O /dev/null http://localhost:6060/v1/namespaces ) ; do sleep 1 ; done
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://localhost:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner --threshold="Negligible" -c http://localhost:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml $CI_REGISTRY_IMAGE:latest
  after_script:
    - cat gl-container-scanning-report.json
  artifacts:
    paths: [gl-container-scanning-report.json]
  only:
    - master
