// config for renovate! not quasar
module.exports = {
  platform: 'gitlab',
  endpoint: 'https://0xacab.org/api/v4/',
  assignees: ['jfriedli'],
  baseBranches: ['develop'],
  labels: ['renovate'],
  extends: ['config:base']
}
