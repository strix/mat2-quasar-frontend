export default {
  remove_metadata: 'Metadaten entfernen',
  the_file_you_see: 'Die sichtbare Datei ist nur die Spitze des Eisbergs. Entferne die versteckten Metadaten mit MAT2',
  info: 'Info',
  simply_drag: 'Drag & Drop, Einfügen oder die Dateiauswahl verwenden.',
  metadata_removed: 'Metadaten entfernt',
  metadata_managed_to_remove: 'Metadaten erfolgreich entfernt',
  removal_failed: 'Entfernen der Metadaten fehlgeschlagen',
  could_not_clean_files: 'Folgenden Dateien konnten nicht gesäubert werden:',
  loading_failed: 'Laden fehlgeschlagen',
  four_o_four: 'Hier gibt es kein Garn.',
  go_home: 'Zurück',
  mat_locally: 'MAT2 lokal',
  mat_locally_info: 'Auch wenn wir keine Kopie deiner Datei speichern, kannst du dir dessen nie sicher sein: Handle entsprechend. Folglich ist es besser, MAT2 lokal auf deinem Gerät auszuführen.',
  mat_pip: 'MAT2 ist verfügbar unter pip',
  mat_debian: 'MAT2 auf Debian',
  mat_debian_available: 'MAT2 ist verfügbar auf Debian',
  more_info: 'Mehr Infos:',
  supported_formats: 'Unterstütze Dateiformate',
  bulk_download: 'Download',
  error_bulk_download_creation: 'Erstellen der Zip Datei ist fehlgeschlagen!',
  general_error: 'O Ooooh, etwas ist schief gelaufen',
  error_report: 'Wenn du diesen Fehler weiterhin erhältst, melde ihn bitte <a rel="noreferrer" href="https://0xacab.org/jfriedli/mat2-quasar-frontend/issues">hier</a>',
  MAT2_metadata: 'Was sind Metadaten?',
  mat_what_is_metadata_1: 'Metadaten bestehen aus Informationen, die deine Datei charakterisieren.\n' +
    'Sie beantworten die Hintergrundfragen wer, wie, wenn und was.\n' +
    'Deine Datei erhält so eine facettenreiche Dokumentation.',
  mat_what_is_metadata_2: 'Die Metadaten deiner Datei geben viel über dich Preis.\n' +
    'Zum Beispiel speichern Kameras Dateiinformationen über den Zeitpunkt einer Aufnahme und welche Kamera dafür verwendet wurde.\n' +
    'Dokumente, wie PDF oder Docx, fügen automatisch Informationen zu Autor*in oder Unternehmen zum Dokument hinzu.\n' +
    'Du willst all diese Informationen gar nicht veröffentlichen?',
  mat_what_is_metadata_3: 'Hier kann dir MAT2 helfen: Es beseitigt so viele Metadaten wie möglich\n',
  show_removed_metadata: 'Details über Metadaten anzeigen',
  show_remaining_metadata: 'Übrige Metadaten anzeigen',
  no_removed_no_remaining_metadata: 'Glückwunsch, es gibt nichts anzuzeigen!',
  removed_metadata: 'Entfernte Metadaten',
  remaining_metadata: 'Verbleibende Metadaten',
  label: 'Bezeichnung',
  value: 'Wert',
  of: 'von',
  records_per_page: 'Einträge pro Seite'
}
