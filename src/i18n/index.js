import enUS from './en-us'
import deDe from './de-de'
import frFR from './fr-fr'
import esES from './es-es'
import itIt from './it-it'

export default {
  en_US: enUS,
  de_DE: deDe,
  fr_FR: frFR,
  es_ES: esES,
  it_IT: itIt
}
