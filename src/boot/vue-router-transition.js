import VuePageTransition from 'vue-page-transition'

export default async ({ app, Vue }) => {
  Vue.use(VuePageTransition)
}
